"use strict"; //aplica o modo estrito para o arquivo todo
exports._esModule = true
var Notas_do_aluno = (function () {
    function Notas_do_aluno(nome, matricula, curso, notas){
        this.nome = nome;
        this.matricula = matricula;
        this.curso = curso;
        this.notas = notas;
    }
    return Notas_do_aluno;
}());
exports.Notas_do_aluno = Notas_do_aluno
